package com.epam.trainings.main;

import com.epam.trainings.mvc.controller.ViewController;
import com.epam.trainings.mvc.controller.ViewControllerImpl;
import com.epam.trainings.mvc.model.Menu;
import com.epam.trainings.mvc.model.beans.BeanB;
import com.epam.trainings.mvc.model.beans.BeanC;
import com.epam.trainings.mvc.model.beans.BeanD;
import com.epam.trainings.mvc.model.beans.BeanF;
import com.epam.trainings.mvc.view.View;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

public class Main {
  public static void main(String[] args) {
    Menu menu = new Menu();
    View view = new View(menu);
    ViewController controller = new ViewControllerImpl(view);
    view.startBySettingUpController(controller);
  }
}
