package com.epam.trainings.mvc.model.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
  private static Logger log = LogManager.getLogger(CustomBeanPostProcessor.class.getName());

  public Object postProcessBeforeInitialization(Object bean, String beanName)
      throws BeansException {
    log.info("Called postProcessBeforeInitialization() for :" + bean);
    return bean;
  }

  public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    log.info("Called postProcessAfterInitialization() for :" + beanName);
    if (bean instanceof CustomBeanValidator) {
      ((CustomBeanValidator) bean).validate();
    }
    return bean;
  }
}
