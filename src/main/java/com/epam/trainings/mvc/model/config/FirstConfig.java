package com.epam.trainings.mvc.model.config;

import com.epam.trainings.mvc.model.beans.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@Import(SecondConfig.class)
@PropertySource("beans.properties")
public class FirstConfig {
  String desc = "[Fully depends on BeanA] = ";

  @Bean
  public BeanA getFirstTypeBeanA(BeanB beanB, BeanC beanC) {
    return new BeanA(beanB.getName(), beanC.getValue());
  }

  @Bean("secondBeanA")
  public BeanA getSecondTypeBeanA(BeanB beanB, BeanD beanD) {
    return new BeanA(beanB.getName(), beanD.getValue());
  }

  @Bean("thirdBeanA")
  public BeanA getThirdTypeBeanA(BeanC beanC, BeanD beanD) {
    return new BeanA(beanC.getName(), beanD.getValue());
  }

  @Bean("getAbsoluteBeanE")
  public BeanE getAbsoluteBeanE(BeanA getFirstTypeBeanA) {
    return new BeanE(desc + getFirstTypeBeanA.getName(), getFirstTypeBeanA.getValue());
  }

  @Bean
  public BeanE getSecondBeanE(@Qualifier("secondBeanA") BeanA beanA) {
    return new BeanE(desc + beanA.getName(), beanA.getValue());
  }

  @Bean
  public BeanE getAnotherBeanE(@Qualifier("thirdBeanA") BeanA beanA) {
    return new BeanE(desc + beanA.getName(), beanA.getValue());
  }

  @Bean
  @Lazy
  public BeanF getBeanF(@Value("${beanF.name}") String name, @Value("${beanF.value}") int value) {
    return new BeanF(name, value);
  }

  @Bean
  public CustomBeanPostProcessor myBeanPostProcessor() {
    return new CustomBeanPostProcessor();
  }

  @Bean
  public CustomBeanFactoryPostProcessor getMyBeanFactory() {
    return new CustomBeanFactoryPostProcessor();
  }
}
