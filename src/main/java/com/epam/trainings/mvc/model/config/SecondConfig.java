package com.epam.trainings.mvc.model.config;

import com.epam.trainings.mvc.model.beans.BeanB;
import com.epam.trainings.mvc.model.beans.BeanC;
import com.epam.trainings.mvc.model.beans.BeanD;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("beans.properties")
public class SecondConfig {

  @Value("${beanD.name}")
  private String beanDname;

  @Value("${beanD.value}")
  private int beanDvalue;

  @Value("${beanC.name}")
  private String beanCname;

  @Value("${beanC.value}")
  private int beanCvalue;

  @Value("${beanB.name}")
  private String beanBname;

  @Value("${beanB.value}")
  private int beanBvalue;

  @Bean(initMethod = "init", destroyMethod = "destroy")
  @DependsOn({"beanD", "beanB"})
  public BeanC getBeanC() {
    return new BeanC(beanCname, beanCvalue);
  }

  @Bean(name = "beanD", initMethod = "init", destroyMethod = "destroy")
  public BeanD getBeanD() {
    return new BeanD(beanDname, beanDvalue);
  }

  @Bean(name = "beanB", initMethod = "init", destroyMethod = "destroy")
  public BeanB getBeanB() {
    return new BeanB(beanBname, beanBvalue);
  }
}
