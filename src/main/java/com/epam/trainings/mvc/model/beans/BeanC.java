package com.epam.trainings.mvc.model.beans;

import com.epam.trainings.mvc.model.config.SecondConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.validation.constraints.NotNull;

@PropertySource("beans.properties")
public class BeanC implements Validator, CustomBeanValidator {

  private static Logger log = LogManager.getLogger(BeanC.class.getName());

  @NotNull private String name;
  private int value;

  public BeanC(String name, int value) {
    this.name = name;
    this.value = value;
  }

  public BeanC() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public void validate() {
    if (getValue() < 0) {
      log.error("Bean validation error, value must be positive!");
    }
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("BeanC = ");
    sb.append("[name = " + name);
    sb.append(",vaue = " + value);
    return sb.toString();
  }

  @Override
  public boolean supports(Class<?> aClass) {
    return BeanC.class.equals(aClass);
  }

  @Override
  public void validate(Object o, Errors errors) {
    BeanC bean = (BeanC) o;
    if (bean.getValue() < 0) {
      errors.rejectValue("value", "value.negative");
    }
  }

  public void init() {
    log.info("Initializing bean - " + getClass().getName());
  }

  public void destroy() {
    log.warn("Destroying bean - " + getClass().getName());
  }
}
