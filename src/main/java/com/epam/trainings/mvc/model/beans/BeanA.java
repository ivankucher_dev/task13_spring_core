package com.epam.trainings.mvc.model.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.validation.constraints.NotNull;

public class BeanA implements Validator, CustomBeanValidator, InitializingBean, DisposableBean {
  private static Logger log = LogManager.getLogger(BeanA.class.getName());
  @NotNull private String name;
  private int value;

  public BeanA(String name, int value) {
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("BeanA = ");
    sb.append("[name = " + name);
    sb.append(",vaue = " + value);
    return sb.toString();
  }

  @Override
  public boolean supports(Class<?> aClass) {
    return BeanA.class.equals(aClass);
  }

  @Override
  public void validate(Object o, Errors errors) {
    BeanA bean = (BeanA) o;
    if (bean.getValue() < 0) {
      errors.rejectValue("value", "value.negative");
    }
  }

  @Override
  public void destroy() throws Exception {
    System.out.println("Destroying bean - " + getClass().getSimpleName());
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    System.out.println(
        "Init method after properties are set : "
            + name
            + ", bean name - "
            + getClass().getSimpleName());
  }

  @Override
  public void validate() {
    if (getValue() < 0) {
      log.error("Bean validation error, value must be positive!");
    }
  }
}
