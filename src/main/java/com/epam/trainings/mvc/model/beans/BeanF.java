package com.epam.trainings.mvc.model.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.validation.constraints.NotNull;

@PropertySource("beans.properties")
public class BeanF implements Validator, CustomBeanValidator {

  private static Logger log = LogManager.getLogger(BeanF.class.getName());
  @NotNull private String name;
  private int value;

  public BeanF(String name, int value) {
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public void validate() {
    if (getValue() < 0) {
      log.error("Bean validation error, value must be positive!");
    }
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("BeanF = ");
    sb.append("[name = " + name);
    sb.append(",vaue = " + value);
    return sb.toString();
  }

  @Override
  public boolean supports(Class<?> aClass) {
    return BeanF.class.equals(aClass);
  }

  @Override
  public void validate(Object o, Errors errors) {
    BeanF bean = (BeanF) o;
    if (!bean.getName().equalsIgnoreCase("BeanF")) {
      errors.rejectValue("name", "name.negative");
    }
  }
}
