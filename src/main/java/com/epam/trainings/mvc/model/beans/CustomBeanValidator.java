package com.epam.trainings.mvc.model.beans;

public interface CustomBeanValidator {
  void validate();
}
