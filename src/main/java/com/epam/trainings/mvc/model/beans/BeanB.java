package com.epam.trainings.mvc.model.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.validation.constraints.NotNull;

@PropertySource("beans.properties")
public class BeanB implements Validator, CustomBeanValidator {
  private static Logger log = LogManager.getLogger(BeanB.class.getName());

  @NotNull private String name;
  private int value;

  public BeanB(String name, int value) {
    this.name = name;
    this.value = value;
  }

  public BeanB() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("BeanB = ");
    sb.append("[name = " + name);
    sb.append(",vaue = " + value);
    return sb.toString();
  }

  @Override
  public void validate() {
    if (getValue() < 0) {
      log.error("Bean validation error, value must be positive!");
    }
  }

  @Override
  public boolean supports(Class<?> aClass) {
    return BeanB.class.equals(aClass);
  }

  @Override
  public void validate(Object o, Errors errors) {
    BeanB bean = (BeanB) o;
    if (bean.getValue() < 0) {
      errors.rejectValue("value", "value.negative");
    }
  }

  public void init() {
    log.info("Initializing bean - " + getClass().getName());
  }

  public void anotheIinit() {
    log.info("Initializing bean from another init - " + getClass().getName());
  }

  public void destroy() {
    log.warn("Destroying bean - " + getClass().getName());
  }
}
