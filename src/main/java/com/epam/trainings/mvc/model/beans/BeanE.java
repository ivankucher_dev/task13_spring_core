package com.epam.trainings.mvc.model.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.validation.constraints.NotNull;

public class BeanE implements Validator, CustomBeanValidator {

  private static Logger log = LogManager.getLogger(BeanE.class.getName());
  @NotNull private String name;
  private int value;

  public BeanE(String name, int value) {
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public void validate() {
    if (getValue() < 0) {
      log.error("Bean validation error, value must be positive!");
    }
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("BeanE = ");
    sb.append("[name = " + name);
    sb.append(",vaue = " + value);
    return sb.toString();
  }

  @PostConstruct
  public void initIt() throws Exception {
    System.out.println("Init method after properties are set : " + name);
  }

  @PreDestroy
  public void cleanUp() throws Exception {
    System.out.println("Spring Container is destroy!" + getClass().getSimpleName() + " clean up");
  }

  @Override
  public boolean supports(Class<?> aClass) {
    return BeanE.class.equals(aClass);
  }

  @Override
  public void validate(Object o, Errors errors) {
    BeanE bean = (BeanE) o;
    if (bean.getValue() < 0) {
      errors.rejectValue("value", "value.negative");
    }
  }
}
