package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.beans.*;
import com.epam.trainings.mvc.model.config.FirstConfig;
import com.epam.trainings.mvc.model.config.SecondConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class CheckLifecycleCommand implements Command {
  private static Logger log = LogManager.getLogger(CheckLifecycleCommand.class.getName());

  @Override
  public void execute() {
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    context.register(SecondConfig.class);
    context.register(FirstConfig.class);
    context.refresh();
    BeanA beanA =(BeanA) context.getBean("secondBeanA");
    BeanB beanB = context.getBean(BeanB.class);
    BeanC beanC = context.getBean(BeanC.class);
    BeanD beanD = context.getBean(BeanD.class);
    BeanF beanF = context.getBean(BeanF.class);
    BeanE beanE = (BeanE) context.getBean("getAbsoluteBeanE");
    BeanE beanEE = (BeanE) context.getBean("getSecondBeanE");
    BeanE beanEEE = (BeanE) context.getBean("getAnotherBeanE");
  }
}
